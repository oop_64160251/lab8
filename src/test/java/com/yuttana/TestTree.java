package com.yuttana;

public class TestTree {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0;
    public static final int MIN_Y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public TestTree(String name, char symbol, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.symbol = symbol;
    }

    public void print(){
        System.out.println(name + " X: " + x + " Y: " + y);
    }
    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
