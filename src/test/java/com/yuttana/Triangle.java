package com.yuttana;

import com.yuttana.lab8.TestShape;

public class Triangle {
    private String name;
    private int a;
    private int b;
    private int c;
    public Triangle (String name, int a, int b, int c){
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public void printTriangleArea(){
        int s = (a+b+c)/2;
        System.out.println(name + " = " + Math.sqrt(s*(s-a)*(s-b)*(s-c)));
    }

    public void printTrianglePerimeter(){
        int A = a+b+c;
        System.out.println("TrianglePerimeter =" + A);
    }
        public static void main(String[] args) {
            Triangle triangle = new Triangle("TriangleArea", 5, 5, 6);
            triangle.printTriangleArea();
            triangle.printTrianglePerimeter();
    }
}
