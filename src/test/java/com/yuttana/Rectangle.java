package com.yuttana;


public class Rectangle {
    private String name;
    private int width;
    private int height;
    Rectangle(String name, int width, int height){
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public void printRectanglePerimeter() {
        System.out.println(name + " = " + ((width*2)+(height*2)));
    }

    public void printRectangleArea(){
        System.out.println(name + " = " + width*height);
    }

    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle("rect1", 10, 5);
        Rectangle rect2 = new Rectangle("rect2", 5, 3);
        rect1.printRectangleArea();
        rect2.printRectangleArea();
        rect1.printRectanglePerimeter();
        rect2.printRectanglePerimeter();
    }
}
