package com.yuttana;

import junit.framework.Test;

public class Tree {
    public static void main(String[] args) {
        TestTree tree1 = new TestTree("tree1", 't' , 5 , 10);
        TestTree tree2 = new TestTree("tree2", 'T' , 5, 11);
        tree1.print();
        tree2.print();
        for (int y = TestTree.MIN_Y; y <= TestTree.MAX_Y; y++) {
            for (int x = TestTree.MIN_X; x <= TestTree.MAX_X; x++) {
                if (tree1.getX() == x && tree1.getY() == y) {
                    System.out.print(tree1.getSymbol());
                }
                else if (tree2.getX() == x && tree2.getY() == y) {
                    System.out.print(tree2.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
