package com.yuttana;

import com.yuttana.lab8.TestShape;

public class Circle {
    private int radius;
    private String name;
    public Circle (String name, int radius){
        this.name = name;
        this.radius = radius;
    }
    public void printCircleArea(){
        System.out.println(name + " = " + (3.14*radius*radius));
    }

    public void printCirclePerimeter(){
        System.out.println(name + " = " + (2*3.14*radius));
    }
    public static void main(String[] args) {
            Circle circle1 = new Circle("Circle1", 1);
            Circle circle2 = new Circle("Circle2", 2);
            circle1.printCircleArea();
            circle2.printCircleArea();
            circle1.printCirclePerimeter();
            circle2.printCirclePerimeter();
        
    }
}
